package com.textract.api.view.controllers;


import com.textract.api.domain.services.CleansingService;
import com.textract.api.view.responses.CleanedIDResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class ExtractionController {

    private final CleansingService cleansingService;

    public ExtractionController(CleansingService cleansingService) {
        this.cleansingService = cleansingService;
    }

    @GetMapping(path = "/hello")
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("Hello World!");
    }


    @GetMapping(path = "/execute")
    public ResponseEntity<String> authenticate(@RequestParam String code) {
        return ResponseEntity.ok("Hello World!");
    }

    @PostMapping(path = "/upload")
    public ResponseEntity<CleanedIDResponse> upload(@RequestHeader(name = "Authorization") String token,
                                                    @RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(cleansingService.extractText(file));
    }
}
