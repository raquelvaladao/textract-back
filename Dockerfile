# FROM adoptopenjdk/openjdk11:ubi
FROM fabric8/java-alpine-openjdk11-jre

WORKDIR /app

ENV APP_HOME app
ENV APP_NAME api.jar

COPY target/*.jar ${APP_HOME}/${APP_NAME}

EXPOSE 8080

ENTRYPOINT java -jar ${APP_HOME}/${APP_NAME}